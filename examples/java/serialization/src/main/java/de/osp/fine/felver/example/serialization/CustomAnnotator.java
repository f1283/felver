package de.osp.fine.felver.example.serialization;

import org.jsonschema2pojo.AbstractAnnotator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JFieldVar;

public class CustomAnnotator extends AbstractAnnotator {

	@Override
	public void propertyField(JFieldVar field, JDefinedClass clazz, String propertyName, JsonNode propertyNode) {
		super.propertyField(field, clazz, propertyName, propertyNode);

		if (propertyNode.has("isEncryptionEnabled") && propertyNode.get("isEncryptionEnabled").asBoolean(false)) {
			field.annotate(JsonSerialize.class).param("using", EncryptSerializer.class);
			field.annotate(JsonDeserialize.class).param("using", DecryptSerializer.class);
		}
	}
}
