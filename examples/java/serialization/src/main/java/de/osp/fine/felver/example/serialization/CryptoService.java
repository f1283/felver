package de.osp.fine.felver.example.serialization;

import org.springframework.stereotype.Component;

import de.osp.fine.felver.CryptoUtils;

@Component
public class CryptoService {
    private final CryptoUtils cryptoUtils;

    public CryptoService(KeyProperties keyProperties) {
        cryptoUtils = new CryptoUtils(keyProperties.getProjectId(), keyProperties.getLocationId(),
                keyProperties.getKeyRingId(), keyProperties.getKeyId());
    }

    public CryptoUtils getCryptoUtils() {
        return cryptoUtils;
    }
}
