package de.osp.fine.felver.example.serialization;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DecryptSerializer extends JsonDeserializer<String> {
    private static CryptoService cryptoService;

    @Autowired
    public void setCryptoService(CryptoService cryptoService){
        DecryptSerializer.cryptoService = cryptoService;
    }

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        try {
            return cryptoService.getCryptoUtils().decrypt(p.getValueAsString());
        } catch (GeneralSecurityException e) {
            throw new IOException(e);
        }
    }
}
