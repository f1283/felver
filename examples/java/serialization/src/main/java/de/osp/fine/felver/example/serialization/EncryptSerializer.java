package de.osp.fine.felver.example.serialization;

import java.io.IOException;
import java.security.GeneralSecurityException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EncryptSerializer extends JsonSerializer<String> {
    private static CryptoService cryptoService;

    @Autowired
    public void setCryptoService(CryptoService cryptoService){
        EncryptSerializer.cryptoService = cryptoService;
    }

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        try {
            gen.writeObject(cryptoService.getCryptoUtils().encrypt(value));
        } catch (GeneralSecurityException e) {
            throw new IOException(e);
        }
    }
}
