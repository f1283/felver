package de.osp.fine.felver.example.serialization;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "kms")
public class KeyProperties {
	private String projectId;
	private String locationId;
	private String keyRingId;
	private String keyId;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getKeyRingId() {
		return keyRingId;
	}

	public void setKeyRingId(String keyRingId) {
		this.keyRingId = keyRingId;
	}

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}
}
