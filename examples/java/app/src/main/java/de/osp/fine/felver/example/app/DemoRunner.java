package de.osp.fine.felver.example.app;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import de.osp.fine.felver.example.app.model.DemoMessage;

@Component
public class DemoRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        DemoMessage demoMessage = new DemoMessage();
        demoMessage.setFirstName("Max");
        demoMessage.setLastName("Mustermann");
        demoMessage.setSomeOtherValue("Hello World");

        System.out.println("===> Original object: " + demoMessage.toString());

        String encryptedDemoMessage = objectMapper.writeValueAsString(demoMessage);

        System.out.println("===> Encrypted JSON: " + encryptedDemoMessage);

        DemoMessage decryptedDemoMessage = objectMapper.readValue(encryptedDemoMessage, DemoMessage.class);

        System.out.println("===> Decrypted object: " + decryptedDemoMessage.toString());
    }
}
