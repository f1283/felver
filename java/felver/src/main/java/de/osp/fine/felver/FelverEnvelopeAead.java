package de.osp.fine.felver;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.time.Clock;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class FelverEnvelopeAead {
    static final int IV_LENGTH_BYTE = 12;
    static final int TAG_LENGTH_BIT = 128;
    static final int FIELD_ENCRYPTED_LEN_BYTE = 4;
    static final int FIELD_ALGORITHM_LENGTH_BYTE = 1;

    private static final String ALGORITHM = "AES/GCM/NoPadding";
    private static final String SECRET_KEY_ALGORITH = "AES";
    private static final int ALGORITHM_ID = 1;
    private static final long KEY_RENEWAL_AFTER_USAGE = 4294967296L;

    private final SecureRandom secureRandom = new SecureRandom();

    private final EncryptionKeyStorage encryptionKeyStorage;
    private final DecryptionKeyStorage decryptionKeyStorage;

    public FelverEnvelopeAead(final KeyProvider keyProvider) throws Exception {
        this(new EncryptionKeyStorage(keyProvider, KEY_RENEWAL_AFTER_USAGE, Clock.systemUTC()),
                new DecryptionKeyStorage(keyProvider, Clock.systemUTC()));
    }

    FelverEnvelopeAead(final EncryptionKeyStorage encryptionKeyStorage,
            final DecryptionKeyStorage decryptionKeyStorage) {
        this.encryptionKeyStorage = encryptionKeyStorage;
        this.decryptionKeyStorage = decryptionKeyStorage;
    }

    public String encrypt(final String plaintext) throws Exception {
        return Base64.getEncoder().encodeToString(encrypt(plaintext.getBytes()));
    }

    public String decrypt(final String value) throws Exception {
        return new String(decrypt(Base64.getDecoder().decode(value)));
    }

    public byte[] encrypt(final byte[] plaintext) throws Exception {
        final EncryptionKey encryptionKey = encryptionKeyStorage.getEncryptionKey();
        encryptionKey.incrementUsageCount();

        final byte[] iv = new byte[IV_LENGTH_BYTE];
        secureRandom.nextBytes(iv);

        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        final GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH_BIT, iv);
        final SecretKey key = new SecretKeySpec(encryptionKey.getPlaintextKey().get(), SECRET_KEY_ALGORITH);
        cipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec);
        final byte[] ciphertext = cipher.doFinal(plaintext);

        final byte algorithm = 1;
        final byte[] encryptedKey = encryptionKey.getEncryptedKey().get();
        final byte[] encryptedKeyLength = ByteBuffer.allocate(FIELD_ENCRYPTED_LEN_BYTE).putInt(encryptedKey.length).array();

        final ByteBuffer byteBuffer = ByteBuffer.allocate(1 + encryptedKeyLength.length + encryptedKey.length + iv.length + ciphertext.length);
        byteBuffer.put(algorithm);
        byteBuffer.put(encryptedKeyLength);
        byteBuffer.put(encryptedKey);
        byteBuffer.put(iv);
        byteBuffer.put(ciphertext);

        return byteBuffer.array();
    }

    public byte[] decrypt(final byte[] value) throws Exception {
        final ByteBuffer buffer = ByteBuffer.wrap(value);

        final int algorithm = buffer.get();
        if (algorithm != ALGORITHM_ID) {
            throw new GeneralSecurityException();
        }

        final int encryptedKeyLength = buffer.getInt();
        if (encryptedKeyLength <= 0 || encryptedKeyLength > value.length - FIELD_ENCRYPTED_LEN_BYTE) {
            throw new GeneralSecurityException();
        }

        final byte[] encryptedKey = new byte[encryptedKeyLength];
        buffer.get(encryptedKey, 0, encryptedKeyLength);

        final byte[] iv = new byte[IV_LENGTH_BYTE];
        buffer.get(iv, 0, IV_LENGTH_BYTE);

        final byte[] payload = new byte[buffer.remaining()];
        buffer.get(payload, 0, buffer.remaining());

        final DecryptionKey decryptionKey = decryptionKeyStorage.getDecryptionKey(Bytes.wrap(encryptedKey));

        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        final GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH_BIT, iv);
        final SecretKey key = new SecretKeySpec(decryptionKey.getPlaintextKey().get(), SECRET_KEY_ALGORITH);
        cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
        final byte[] plaintext = cipher.doFinal(payload);

        return plaintext;
    }
}
