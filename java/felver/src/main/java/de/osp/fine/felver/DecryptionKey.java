package de.osp.fine.felver;

import java.time.Clock;

class DecryptionKey extends Key {
    public DecryptionKey(final Bytes plaintextKey, final Clock clock) {
        super(plaintextKey, clock);
    }
}
