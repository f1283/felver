package de.osp.fine.felver;

import java.time.Clock;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpStatus;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpBackOffIOExceptionHandler;
import com.google.api.client.http.HttpBackOffUnsuccessfulResponseHandler;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.BackOff;
import com.google.api.client.util.Data;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.client.util.Key;
import com.google.auth.http.HttpCredentialsAdapter;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.IdTokenCredentials;
import com.google.auth.oauth2.IdTokenProvider;

public class KmsProxyKeyProvider implements KeyProvider {
    private static final GsonFactory gsonFactory = new GsonFactory();
    private static final JsonObjectParser parser = new JsonObjectParser(gsonFactory);

    private final IdTokenCredentials tokenCredential;
    private final String baseUrl;

    private static final List<Integer> HTTP_STATUS_CODES_VALID_FOR_RETRY = Collections.unmodifiableList(Arrays.asList(
            HttpStatus.SC_REQUEST_TIMEOUT,
            HttpStatus.SC_TOO_MANY_REQUESTS,
            HttpStatus.SC_INTERNAL_SERVER_ERROR,
            HttpStatus.SC_BAD_GATEWAY,
            HttpStatus.SC_SERVICE_UNAVAILABLE,
            HttpStatus.SC_GATEWAY_TIMEOUT));

    private static final int DEFAULT_NUMBER_OF_RETRIES = 10;
    private int numberOfRetries = DEFAULT_NUMBER_OF_RETRIES;

    private static final int INITIAL_INTERVAL_MILLIS = 500;
    private static final double RANDOMIZATION_FACTOR = 0.5;
    private static final double MULTIPLIER = 1.5;
    private static final int MAX_INTERVAL_MILLIS = 15000;
    private static final int MAX_ELAPSED_TIME_MILLIS = Integer.MAX_VALUE;

    public KmsProxyKeyProvider(final GoogleCredentials credentials, final String baseUrl, final String audience) {
        if (!(credentials instanceof IdTokenProvider)) {
            throw new IllegalArgumentException("Credentials are not an instance of IdTokenProvider.");
        }

        this.tokenCredential = IdTokenCredentials.newBuilder()
                .setIdTokenProvider((IdTokenProvider) credentials)
                .setTargetAudience(audience)
                .build();

        this.baseUrl = baseUrl;
    }

    private BackOff createBackOff() {
        return new ExponentialBackOff.Builder()
                .setInitialIntervalMillis(INITIAL_INTERVAL_MILLIS)
                .setRandomizationFactor(RANDOMIZATION_FACTOR)
                .setMultiplier(MULTIPLIER)
                .setMaxIntervalMillis(MAX_INTERVAL_MILLIS)
                .setMaxElapsedTimeMillis(MAX_ELAPSED_TIME_MILLIS)
                .build();
    }

    private HttpBackOffUnsuccessfulResponseHandler createUnsuccessfulResponseHandler() {
        final HttpBackOffUnsuccessfulResponseHandler handler = new HttpBackOffUnsuccessfulResponseHandler(createBackOff());
        handler.setBackOffRequired(response -> HTTP_STATUS_CODES_VALID_FOR_RETRY.contains(response.getStatusCode()));
        return handler;
    }

    public void setNumberOfRetries(int numberOfRetries) {
        if (numberOfRetries < 0) {
            throw new IllegalArgumentException("numberOfRetries must be greater than or equal to 0");
        }
        this.numberOfRetries = numberOfRetries;
    }

    private void setRetrySettings(HttpRequest request) {
        request.setNumberOfRetries(numberOfRetries);
        request.setIOExceptionHandler(new HttpBackOffIOExceptionHandler(createBackOff()));
        request.setUnsuccessfulResponseHandler(createUnsuccessfulResponseHandler());
    }

    @Override
    public EncryptionKey generateNewEncryptionKey(final Clock clock) throws Exception {
        final GenericUrl genericUrl = new GenericUrl(baseUrl + "/v1/keys");
        final HttpCredentialsAdapter adapter = new HttpCredentialsAdapter(tokenCredential);
        final HttpTransport transport = new NetHttpTransport();

        final ByteArrayContent content = ByteArrayContent.fromString("application/json", "");
        final HttpRequest request = transport.createRequestFactory(adapter).buildPostRequest(genericUrl, content);
        request.setParser(parser);
        setRetrySettings(request);
        final HttpResponse response = request.execute();

        final PostEncryptionKeyResponse postEncryptionKeyResponse = response.parseAs(PostEncryptionKeyResponse.class);
        postEncryptionKeyResponse.validate();

        final EncryptionKey encryptionKey = new EncryptionKey(postEncryptionKeyResponse.getPlaintextKeyBytes(),
                postEncryptionKeyResponse.getEncryptedKeyBytes(), clock);

        return encryptionKey;
    }

    @Override
    public DecryptionKey getDecryptionKey(final Clock clock, final Bytes encryptedKey) throws Exception {
        final PostEncryptedKeyRequest postEncryptedKeyRequest = new PostEncryptedKeyRequest();
        postEncryptedKeyRequest.setEncryptedKeyBytes(encryptedKey);

        final GenericUrl genericUrl = new GenericUrl(baseUrl + "/v1/keys/decrypted");
        final HttpCredentialsAdapter adapter = new HttpCredentialsAdapter(tokenCredential);
        final HttpTransport transport = new NetHttpTransport();

        final JsonHttpContent content = new JsonHttpContent(gsonFactory, postEncryptedKeyRequest);
        final HttpRequest request = transport.createRequestFactory(adapter).buildPostRequest(genericUrl, content);
        request.setParser(parser);
        setRetrySettings(request);
        final HttpResponse response = request.execute();

        final PostEncryptedKeyResponse postEncryptedKeyResponse = response.parseAs(PostEncryptedKeyResponse.class);
        postEncryptedKeyResponse.validate();

        final DecryptionKey decryptionKey = new DecryptionKey(postEncryptedKeyResponse.getPlaintextKeyBytes(), clock);

        return decryptionKey;
    }

    static public class PostEncryptedKeyRequest {
        @Key
        private String encryptedKey;

        public void setEncryptedKeyBytes(final Bytes encryptedKey) {
            this.encryptedKey = encryptedKey.toBase64();
        }
    }

    static public class PostEncryptedKeyResponse {
        @Key
        private String plaintextKey;

        public Bytes getPlaintextKeyBytes() {
            return Bytes.wrap(Base64.getDecoder().decode(plaintextKey));
        }

        public void validate() throws Exception {
            if (plaintextKey == null) {
                throw new Exception("plaintextKey is missing");
            }

            if (Data.isNull(plaintextKey)) {
                throw new Exception("plaintextKey is null");
            }
        }
    }

    static public class PostEncryptionKeyResponse {
        @Key
        private String plaintextKey;
        @Key
        private String encryptedKey;

        public Bytes getPlaintextKeyBytes() {
            return Bytes.wrap(Base64.getDecoder().decode(plaintextKey));
        }

        public Bytes getEncryptedKeyBytes() {
            return Bytes.wrap(Base64.getDecoder().decode(encryptedKey));
        }

        public void validate() throws Exception {
            if (plaintextKey == null) {
                throw new Exception("plaintextKey is missing");
            }

            if (Data.isNull(encryptedKey)) {
                throw new Exception("plaintextKey is null");
            }

            if (encryptedKey == null) {
                throw new Exception("encryptedKey is missing");
            }

            if (Data.isNull(plaintextKey)) {
                throw new Exception("encryptedKey is null");
            }

            if (getPlaintextKeyBytes().get().length != 32) {
                throw new Exception("plaintextKey has invalid length");
            }
        }
    }

}
