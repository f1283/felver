package de.osp.fine.felver;

import java.time.Clock;
import java.time.Duration;

class EncryptionKeyStorage {
    static final Duration KEY_RENEWAL_AFTER_DURATION = Duration.ofHours(6);

    private EncryptionKey encryptionKey;

    private final KeyProvider keyProvider;
    private final long keyRenewalAfterUsage;
    private final Clock clock;

    public EncryptionKeyStorage(final KeyProvider keyProvider, final Long keyRenewalAfterUsage, final Clock clock) {
        this.keyProvider = keyProvider;
        this.keyRenewalAfterUsage = keyRenewalAfterUsage;
        this.clock = clock;
    }

    private EncryptionKey generateNewKey() throws Exception {
        return keyProvider.generateNewEncryptionKey(clock);
    }

    public EncryptionKey getEncryptionKey() throws Exception {
        if (encryptionKey == null
                || Duration.between(clock.instant(), encryptionKey.getCreationDate()).abs().compareTo(KEY_RENEWAL_AFTER_DURATION) > 0
                || encryptionKey.getUsageCount() >= keyRenewalAfterUsage) {
            encryptionKey = generateNewKey();
        }

        return encryptionKey;
    }
}
