package de.osp.fine.felver;

import java.time.Clock;

class EncryptionKey extends Key {
    private final Bytes encryptedKey;
    private Long usageCount = 0L;

    public EncryptionKey(final Bytes plaintextKey, final Bytes encryptedKey, final Clock clock) {
        super(plaintextKey, clock);
        this.encryptedKey = encryptedKey;
    }

    public Bytes getEncryptedKey() {
        return encryptedKey;
    }

    public Long getUsageCount() {
        return usageCount;
    }

    public void incrementUsageCount() {
        this.usageCount = usageCount + 1;
    }
}
