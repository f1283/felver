package de.osp.fine.felver;

import java.util.Arrays;

class Bytes {
    private final byte[] bytes;

    private int hashCode;

    public static Bytes wrap(final byte[] bytes) {
        if (bytes == null) return null;
        return new Bytes(bytes);
    }

    public Bytes(final byte[] bytes) {
        this.bytes = bytes;
        hashCode = 0;
    }

    public byte[] get() {
        return this.bytes.clone();
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = Arrays.hashCode(bytes);
        }

        return hashCode;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;

        if (this.hashCode() != other.hashCode())
            return false;

        if (other instanceof Bytes)
            return Arrays.equals(this.bytes, ((Bytes) other).get());

        return false;
    }

    public String toBase64() {
        return java.util.Base64.getEncoder().encodeToString(bytes);
    }
}
