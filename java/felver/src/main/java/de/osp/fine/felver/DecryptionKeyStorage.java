package de.osp.fine.felver;

import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

class DecryptionKeyStorage {
    static final Duration KEY_DELETION_AFTER_DURATION = Duration.ofHours(12);

    private final HashMap<Bytes, DecryptionKey> storage = new HashMap<>();

    private final KeyProvider keyProvider;
    private final Clock clock;

    public DecryptionKeyStorage(final KeyProvider keyProvider, final Clock clock) {
        this.keyProvider = keyProvider;
        this.clock = clock;
    }

    private void deleteOldKeys() {
        final Iterator<Entry<Bytes, DecryptionKey>> iterator = storage.entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Bytes, DecryptionKey> entry = iterator.next();
            if (Duration.between(clock.instant(), entry.getValue().getCreationDate()).abs().compareTo(KEY_DELETION_AFTER_DURATION) > 0) {
                iterator.remove();
            }
        }
    }

    private DecryptionKey decryptKey(final Bytes encryptedKey) throws Exception {
        return keyProvider.getDecryptionKey(clock, encryptedKey);
    }

    public DecryptionKey getDecryptionKey(final Bytes encryptedKey) throws Exception {
        deleteOldKeys();

        if (!storage.containsKey(encryptedKey)) {
            final DecryptionKey decryptionKey = decryptKey(encryptedKey);
            storage.put(encryptedKey, decryptionKey);
            return decryptionKey;
        }

        return storage.get(encryptedKey);
    }
}
