package de.osp.fine.felver;

import java.time.Clock;
import java.time.Instant;

abstract class Key {
    private final Bytes plaintextKey;
    private final Instant creationDate;

    public Key(final Bytes plaintextKey, final Clock clock) {
        this.plaintextKey = plaintextKey;
        this.creationDate = clock.instant();
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public Bytes getPlaintextKey() {
        return plaintextKey;
    }
}
