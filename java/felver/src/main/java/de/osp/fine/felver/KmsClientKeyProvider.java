package de.osp.fine.felver;

import java.time.Clock;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.kms.v1.CryptoKeyName;
import com.google.cloud.kms.v1.DecryptResponse;
import com.google.cloud.kms.v1.EncryptResponse;
import com.google.cloud.kms.v1.KeyManagementServiceClient;
import com.google.cloud.kms.v1.KeyManagementServiceSettings;
import com.google.protobuf.ByteString;

public class KmsClientKeyProvider implements KeyProvider {
    private static final String KEY_ALGORITHM = "AES";
    private static final int KEY_SIZE_BIT = 256;

    private final CryptoKeyName keyName;
    private final KeyManagementServiceClient kmsClient;
    private final KeyGenerator keyGenerator;

    public KmsClientKeyProvider(final GoogleCredentials credentials, final String projectId, final String locationId,
            final String keyRingId, final String keyId) throws Exception {
        this(createKmsClient(credentials), createKeyGenerator(),
                CryptoKeyName.of(projectId, locationId, keyRingId, keyId));
    }

    KmsClientKeyProvider(final KeyManagementServiceClient kmsClient, final KeyGenerator keyGenerator,
            final CryptoKeyName keyName) throws Exception {
        this.kmsClient = kmsClient;
        this.keyGenerator = keyGenerator;
        this.keyName = keyName;
    }

    private static KeyGenerator createKeyGenerator() throws Exception {
        final KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_ALGORITHM);
        keyGenerator.init(KEY_SIZE_BIT);
        return keyGenerator;
    }

    private static KeyManagementServiceClient createKmsClient(final GoogleCredentials credentials) throws Exception {
        final KeyManagementServiceSettings keyManagementServiceSettings = KeyManagementServiceSettings.newBuilder()
                .setCredentialsProvider(FixedCredentialsProvider.create(credentials))
                .build();

        return KeyManagementServiceClient.create(keyManagementServiceSettings);
    }

    @Override
    public EncryptionKey generateNewEncryptionKey(final Clock clock) throws Exception {
        final SecretKey generatedKey = keyGenerator.generateKey();

        final byte[] plaintextKey = generatedKey.getEncoded();

        final EncryptResponse response = kmsClient.encrypt(keyName, ByteString.copyFrom(plaintextKey));
        final byte[] encryptedKey = response.getCiphertext().toByteArray();

        final EncryptionKey encryptionKey = new EncryptionKey(Bytes.wrap(plaintextKey), Bytes.wrap(encryptedKey),
                clock);

        return encryptionKey;
    }

    @Override
    public DecryptionKey getDecryptionKey(final Clock clock, final Bytes encryptedKey) throws Exception {
        final DecryptResponse response = kmsClient.decrypt(keyName, ByteString.copyFrom(encryptedKey.get()));

        final DecryptionKey decryptionKey = new DecryptionKey(Bytes.wrap(response.getPlaintext().toByteArray()), clock);

        return decryptionKey;
    }
}
