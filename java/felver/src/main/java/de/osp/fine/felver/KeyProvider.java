package de.osp.fine.felver;

import java.time.Clock;

public interface KeyProvider {
    public EncryptionKey generateNewEncryptionKey(Clock clock) throws Exception;

    public DecryptionKey getDecryptionKey(Clock clock, Bytes encryptedKey) throws Exception;
}
