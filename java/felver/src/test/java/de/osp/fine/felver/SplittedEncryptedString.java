package de.osp.fine.felver;

import java.nio.ByteBuffer;
import java.util.Base64;

public class SplittedEncryptedString {
    private final Bytes encryptedByteArray;
    private final byte algorithm;
    private final int encryptedKeyLength;
    private final Bytes encryptedKey;
    private final Bytes iv;

    public SplittedEncryptedString (String encryptedString) {
        final byte[] decodedEncryptedString = Base64.getDecoder().decode(encryptedString);

        encryptedByteArray = Bytes.wrap(decodedEncryptedString);

        final ByteBuffer encryptedBuffer = ByteBuffer.wrap(decodedEncryptedString);

        algorithm = encryptedBuffer.get();

        encryptedKeyLength = encryptedBuffer.getInt();

        final byte[] encryptedKeyRaw = new byte[encryptedKeyLength];
        encryptedBuffer.get(encryptedKeyRaw, 0, encryptedKeyLength);
        encryptedKey = Bytes.wrap(encryptedKeyRaw);

        final byte[] ivRaw = new byte[FelverEnvelopeAead.IV_LENGTH_BYTE];
        encryptedBuffer.get(ivRaw, 0, FelverEnvelopeAead.IV_LENGTH_BYTE);
        iv = Bytes.wrap(ivRaw);
    }
    
    public Bytes getEncryptedByteArray() {
        return encryptedByteArray;
    }

    public byte getAlgorithm() {
        return algorithm;
    }

    public int getEncryptedKeyLength() {
        return encryptedKeyLength;
    }

    public Bytes getEncryptedKey() {
        return encryptedKey;
    }

    public Bytes getIv() {
        return iv;
    }
 }
