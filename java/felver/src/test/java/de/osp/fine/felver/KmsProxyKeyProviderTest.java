package de.osp.fine.felver;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.serverError;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.SocketException;
import java.time.Clock;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.github.tomakehurst.wiremock.stubbing.Scenario;
import com.google.api.client.http.HttpResponseException;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.IdToken;
import com.google.auth.oauth2.IdTokenProvider;
import com.google.gson.JsonObject;

@WireMockTest(httpPort = 8081)
public class KmsProxyKeyProviderTest {
    private final String bearerPrefix = "testToken_";
    private final String targetAudience = "http://localhost:8081";
    private final String bearerToken = "Bearer " + bearerPrefix + targetAudience;

    private final String newEncryptionKeyPath = "/v1/keys";
    private final String getDecryptionKeyPath = "/v1/keys/decrypted";

    private KmsProxyKeyProvider kmsProxyKeyProvider;
    private Clock clock;

    @BeforeEach
    void setUp() {
        final String baseUrl = "http://localhost:8081";

        final GoogleCredentials credentials = new IdTokenProviderMock();
        clock = mock(Clock.class);

        kmsProxyKeyProvider = new KmsProxyKeyProvider(credentials, baseUrl, targetAudience);
        kmsProxyKeyProvider.setNumberOfRetries(1);

        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));
    }

    private static class KeyPair {
        byte[] plaintextKeyBytes;
        byte[] encryptedKeyBytes;
        String plaintextKey;
        String encryptedKey;

        KeyPair(byte[] plaintextKeyBytes, byte[] encryptedKeyBytes, String plaintextKey, String encryptedKey) {
            this.plaintextKeyBytes = plaintextKeyBytes;
            this.encryptedKeyBytes = encryptedKeyBytes;
            this.plaintextKey = plaintextKey;
            this.encryptedKey = encryptedKey;
        }
    }

    private KeyPair createKeyPair() {
        byte[] plaintextKeyBytes = new byte[32];
        new Random().nextBytes(plaintextKeyBytes);
        String plaintextKey = Base64.getEncoder().encodeToString(plaintextKeyBytes);

        byte[] encryptedKeyBytes = UUID.randomUUID().toString().getBytes();
        String encryptedKey = Base64.getEncoder().encodeToString(encryptedKeyBytes);

        return new KeyPair(plaintextKeyBytes, encryptedKeyBytes, plaintextKey, encryptedKey);
    }

    private void setupRetryScenario(String path, JsonObject mockedResponse, ResponseDefinitionBuilder firstResponse) {
        stubFor(post(path)
                .inScenario("Retry Scenario")
                .whenScenarioStateIs(Scenario.STARTED)
                .willReturn(firstResponse)
                .willSetStateTo("First Attempt Failed"));

        stubFor(post(path)
                .inScenario("Retry Scenario")
                .whenScenarioStateIs("First Attempt Failed")
                .willReturn(okJson(mockedResponse.toString())));
    }

    private void setupRetryFailureScenario(String path, ResponseDefinitionBuilder response) {
        stubFor(post(path)
                .inScenario("Retry Scenario")
                .whenScenarioStateIs(Scenario.STARTED)
                .willReturn(response)
                .willSetStateTo("First Attempt Failed"));

        stubFor(post(path)
                .inScenario("Retry Scenario")
                .whenScenarioStateIs("First Attempt Failed")
                .willReturn(response));
    }

    private JsonObject createNewEncryptionKeyMockedResponse(KeyPair keyPair) {
        JsonObject mockedResponse = new JsonObject();

        if (keyPair.plaintextKey == null) {
            mockedResponse.add("plaintextKey", null);
        } else {
            mockedResponse.addProperty("plaintextKey", keyPair.plaintextKey);
        }

        if (keyPair.encryptedKey == null) {
            mockedResponse.add("encryptedKey", null);
        } else {
            mockedResponse.addProperty("encryptedKey", keyPair.encryptedKey);
        }

        return mockedResponse;
    }

    private void assertNewEncryptionKey(KeyPair keyPair, EncryptionKey encryptionKey) {
        assertEquals(Bytes.wrap(keyPair.encryptedKeyBytes), encryptionKey.getEncryptedKey());
        assertEquals(Bytes.wrap(keyPair.plaintextKeyBytes), encryptionKey.getPlaintextKey());
    }

    @Test
    void testGenerateNewEncryptionKey() throws Exception {
        KeyPair keyPair = createKeyPair();
        JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);
        stubFor(post(newEncryptionKeyPath)
                .withHeader("Authorization", equalTo(bearerToken))
                .willReturn(okJson(mockedResponse.toString())));

        EncryptionKey encryptionKey = kmsProxyKeyProvider.generateNewEncryptionKey(clock);

        assertNewEncryptionKey(keyPair, encryptionKey);
    }

    @Test
    void testGenerateNewEncryptionKeyWithHttpErrorRetry() throws Exception {
        KeyPair keyPair = createKeyPair();
        JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);
        setupRetryScenario(newEncryptionKeyPath, mockedResponse, serverError());

        EncryptionKey encryptionKey = kmsProxyKeyProvider.generateNewEncryptionKey(clock);

        assertNewEncryptionKey(keyPair, encryptionKey);
    }

    @Test
    void testGenerateNewEncryptionKeyWithConnectionResetRetry() throws Exception {
        KeyPair keyPair = createKeyPair();
        JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);
        setupRetryScenario(newEncryptionKeyPath, mockedResponse, aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER));

        EncryptionKey encryptionKey = kmsProxyKeyProvider.generateNewEncryptionKey(clock);

        assertNewEncryptionKey(keyPair, encryptionKey);
    }

    @Test
    void testGenerateNewEncryptionKeyWithHttpErrorRetryFailure() throws Exception {
        setupRetryFailureScenario(newEncryptionKeyPath, serverError());

        assertThrows(HttpResponseException.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyWithConnectionResetRetryFailure() throws Exception {
        setupRetryFailureScenario(newEncryptionKeyPath, aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER));

        assertThrows(SocketException.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyMissingPlaintextKey() throws Exception {
        final byte[] encryptedKeyBytes = UUID.randomUUID().toString().getBytes();
        final String encryptedKey = Base64.getEncoder().encodeToString(encryptedKeyBytes);

        final JsonObject mockedResponse = new JsonObject();
        mockedResponse.addProperty("encryptedKey", encryptedKey);

        stubFor(post(newEncryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyNullPlaintextKey() throws Exception {
        final byte[] encryptedKeyBytes = UUID.randomUUID().toString().getBytes();
        final String encryptedKey = Base64.getEncoder().encodeToString(encryptedKeyBytes);

        final KeyPair keyPair = new KeyPair(null, encryptedKeyBytes, null, encryptedKey);
        final JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);

        stubFor(post(newEncryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyPlaintextKeyWrongLength() throws Exception {
        final byte[] plaintextKeyBytes = new byte[16];
        new Random().nextBytes(plaintextKeyBytes);
        final String plaintextKey = Base64.getEncoder().encodeToString(plaintextKeyBytes);

        final byte[] encryptedKeyBytes = UUID.randomUUID().toString().getBytes();
        final String encryptedKey = Base64.getEncoder().encodeToString(encryptedKeyBytes);

        final KeyPair keyPair = new KeyPair(plaintextKeyBytes, encryptedKeyBytes, plaintextKey, encryptedKey);
        final JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);

        stubFor(post(newEncryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyMissingEncryptedKey() throws Exception {
        final byte[] plaintextKeyBytes = new byte[16];
        new Random().nextBytes(plaintextKeyBytes);
        final String plaintextKey = Base64.getEncoder().encodeToString(plaintextKeyBytes);

        final JsonObject mockedResponse = new JsonObject();
        mockedResponse.addProperty("plaintextKey", plaintextKey);

        stubFor(post(newEncryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    @Test
    void testGenerateNewEncryptionKeyNullEncryptedKey() throws Exception {
        final byte[] plaintextKeyBytes = new byte[16];
        new Random().nextBytes(plaintextKeyBytes);
        final String plaintextKey = Base64.getEncoder().encodeToString(plaintextKeyBytes);

        final KeyPair keyPair = new KeyPair(plaintextKeyBytes, null, plaintextKey, null);
        final JsonObject mockedResponse = createNewEncryptionKeyMockedResponse(keyPair);

        stubFor(post(newEncryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.generateNewEncryptionKey(clock);
        });
    }

    private JsonObject createGetDecryptionKeyExpectedRequest(KeyPair keyPair) {
        final JsonObject expectedRequest = new JsonObject();
        expectedRequest.addProperty("encryptedKey", keyPair.encryptedKey);
        return expectedRequest;
    }

    private JsonObject createGetDecryptionKeyMockedResponse(KeyPair keyPair) {
        final JsonObject mockedResponse = new JsonObject();
        mockedResponse.addProperty("plaintextKey", keyPair.plaintextKey);
        return mockedResponse;
    }

    private void assertDecryptionKey(KeyPair keyPair, DecryptionKey decryptionKey) {
        assertEquals(Bytes.wrap(keyPair.plaintextKeyBytes), decryptionKey.getPlaintextKey());
    }

    @Test
    void testGetDecryptionKey() throws Exception {
        final KeyPair keyPair = createKeyPair();
        final JsonObject expectedRequest = createGetDecryptionKeyExpectedRequest(keyPair);
        final JsonObject mockedResponse = createGetDecryptionKeyMockedResponse(keyPair);

        stubFor(post(getDecryptionKeyPath)
                .withHeader("Authorization", equalTo(bearerToken))
                .withRequestBody(equalToJson(expectedRequest.toString()))
                .willReturn(okJson(mockedResponse.toString())));

        final DecryptionKey decryptionKey = kmsProxyKeyProvider.getDecryptionKey(clock,
                Bytes.wrap(keyPair.encryptedKeyBytes));

        assertDecryptionKey(keyPair, decryptionKey);
    }

    @Test
    void testGetDecryptionKeyWithHttpErrorRetry() throws Exception {
        final KeyPair keyPair = createKeyPair();
        final JsonObject mockedResponse = createGetDecryptionKeyMockedResponse(keyPair);

        setupRetryScenario(getDecryptionKeyPath, mockedResponse, serverError());

        final DecryptionKey decryptionKey = kmsProxyKeyProvider.getDecryptionKey(clock,
                Bytes.wrap(keyPair.encryptedKeyBytes));

        assertDecryptionKey(keyPair, decryptionKey);
    }

    @Test
    void testGetDecryptionKeyWithConnectionResetRetry() throws Exception {
        final KeyPair keyPair = createKeyPair();
        final JsonObject mockedResponse = createGetDecryptionKeyMockedResponse(keyPair);

        setupRetryScenario(getDecryptionKeyPath, mockedResponse, aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER));

        final DecryptionKey decryptionKey = kmsProxyKeyProvider.getDecryptionKey(clock,
                Bytes.wrap(keyPair.encryptedKeyBytes));

        assertDecryptionKey(keyPair, decryptionKey);
    }

    @Test
    void testGetDecryptionKeyWithHttpErrorRetryFailure() throws Exception {
        final Bytes encryptedKeyBytes = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        setupRetryFailureScenario(getDecryptionKeyPath, serverError());

        assertThrows(HttpResponseException.class, () -> {
            kmsProxyKeyProvider.getDecryptionKey(clock, encryptedKeyBytes);
        });
    }

    @Test
    void testGetDecryptionKeyWithConnectionResetRetryFailure() throws Exception {
        final Bytes encryptedKeyBytes = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        setupRetryFailureScenario(getDecryptionKeyPath, aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER));

        assertThrows(SocketException.class, () -> {
            kmsProxyKeyProvider.getDecryptionKey(clock, encryptedKeyBytes);
        });
    }

    @Test
    void testGetDecryptionKeyMissingPlaintextKey() throws Exception {
        final Bytes encryptedKeyBytes = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        final JsonObject mockedResponse = new JsonObject();

        stubFor(post(getDecryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.getDecryptionKey(clock, encryptedKeyBytes);
        });
    }

    @Test
    void testGetDecryptionKeyNullPlaintextKey() throws Exception {
        final Bytes encryptedKeyBytes = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        final JsonObject mockedResponse = new JsonObject();
        mockedResponse.add("plaintextKey", null);

        stubFor(post(getDecryptionKeyPath).willReturn(okJson(mockedResponse.toString())));

        assertThrows(Exception.class, () -> {
            kmsProxyKeyProvider.getDecryptionKey(clock, encryptedKeyBytes);
        });
    }

    @Test
    void testNoIdTokenProvider() {
        final GoogleCredentials credentials = mock(GoogleCredentials.class);
        assertThrows(IllegalArgumentException.class, () -> {
            new KmsProxyKeyProvider(credentials, "http://localhost:8081", "http://localhost:8081");
        });
    }

    public class IdTokenProviderMock extends GoogleCredentials implements IdTokenProvider {
        @Override
        public IdToken idTokenWithAudience(String targetAudience, List<Option> options) throws IOException {
            final IdToken idToken = mock(IdToken.class);
            when(idToken.getTokenValue()).thenReturn(bearerPrefix + targetAudience);
            return idToken;
        }
    }

    @Test
    public void testSetNumberOfRetriesWithNegativeValue() {
        assertThrows(IllegalArgumentException.class, () -> {
            kmsProxyKeyProvider.setNumberOfRetries(-1);
        });
    }
}
