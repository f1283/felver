package de.osp.fine.felver;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.time.Clock;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FelverEnvelopeAeadTest {
    private FelverEnvelopeAead fineEncryption;
    private Clock clock;
    private EncryptionKey encryptionKey;
    private DecryptionKey decryptionKey;
    private EncryptionKeyStorage encryptionKeyStorage;
    private DecryptionKeyStorage decryptionKeyStorage;

    @BeforeEach
    void setUp() throws Exception {
        clock = mock(Clock.class);
        encryptionKeyStorage = mock(EncryptionKeyStorage.class);
        decryptionKeyStorage = mock(DecryptionKeyStorage.class);

        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));

        final Bytes encryptedKey = Bytes.wrap("test encrypted key".getBytes());

        final byte[] plaintextKeyRaw = new byte[32];
        new Random().nextBytes(plaintextKeyRaw);
        final Bytes plaintextKey = Bytes.wrap(plaintextKeyRaw);

        encryptionKey = new EncryptionKey(plaintextKey, encryptedKey, clock);
        when(encryptionKeyStorage.getEncryptionKey()).thenReturn(encryptionKey);

        decryptionKey = new DecryptionKey(plaintextKey, clock);
        when(decryptionKeyStorage.getDecryptionKey(eq(encryptedKey))).thenReturn(decryptionKey);

        fineEncryption = new FelverEnvelopeAead(encryptionKeyStorage, decryptionKeyStorage);
    }

    @Test
    void testOriginalStringMatchDecryptedString() throws Exception {
        final String randomString = UUID.randomUUID().toString();

        final String encrypted = fineEncryption.encrypt(randomString);
        final String decrypted = fineEncryption.decrypt(encrypted);

        assertNotEquals(randomString, encrypted, "Original string and encrypted string match.");
        assertEquals(randomString, decrypted, "Original string and decrypted string do not match.");
    }

    @Test
    void testOriginalBytesMatchDecryptedBytes() throws Exception {
        final byte[] randomBytes = UUID.randomUUID().toString().getBytes();

        final byte[] encrypted = fineEncryption.encrypt(randomBytes);
        final byte[] decrypted = fineEncryption.decrypt(encrypted);

        assertFalse(Arrays.equals(randomBytes, encrypted), "Original bytes and encrypted bytes match.");
        assertArrayEquals(randomBytes, decrypted, "Original bytes and decrypted bytes do not match.");
    }

    @Test
    void testCorrectEncryptedStringStructure() throws Exception {
        final String randomString = UUID.randomUUID().toString();

        final String encryptedString = fineEncryption.encrypt(randomString);
        final SplittedEncryptedString splittedEncryptedString = new SplittedEncryptedString(encryptedString);

        assertEquals(splittedEncryptedString.getAlgorithm(), 1, "The first byte of the encrypted string is not 1.");

        assertEquals(splittedEncryptedString.getEncryptedKey(), encryptionKey.getEncryptedKey(), "The encrypted key is incorrect.");
    }

    @Test
    void testIvNotReused() throws Exception {
        final String randomString = UUID.randomUUID().toString();

        final String encryptedString1 = fineEncryption.encrypt(randomString);
        final SplittedEncryptedString splittedEncryptedString1 = new SplittedEncryptedString(encryptedString1);

        final String encryptedString2 = fineEncryption.encrypt(randomString);
        final SplittedEncryptedString splittedEncryptedString2 = new SplittedEncryptedString(encryptedString2);

        assertNotEquals(splittedEncryptedString1.getIv(), splittedEncryptedString2.getIv(), "The same IV was used for two encryptions.");
    }

    @Test
    void testWrongAlgorithmThrowsExceptionOnDecrypt() {
        final byte wrongAlgorithm = 2;

        final byte[] byteArray = new byte[1];
        byteArray[0] = wrongAlgorithm;

        final String wrongEncryptedString = Base64.getEncoder().encodeToString(byteArray);

        assertThrows(GeneralSecurityException.class, () -> fineEncryption.decrypt(wrongEncryptedString));
    }

    @Test
    void testTooShortKeyLengthThrowsExceptionOnDecrypt() {
        final byte algorithm = 1;
        final int encryptedKeyLength = 0;

        ByteBuffer byteBuffer = ByteBuffer.allocate(1 + FelverEnvelopeAead.FIELD_ENCRYPTED_LEN_BYTE);
        byteBuffer.put(algorithm);
        byteBuffer.putInt(encryptedKeyLength);

        final String wrongEncryptedString = Base64.getEncoder().encodeToString(byteBuffer.array());

        assertThrows(GeneralSecurityException.class, () -> fineEncryption.decrypt(wrongEncryptedString));
    }

    @Test
    void testTooLongKeyLengthThrowsExceptionOnDecrypt() {
        final byte algorithm = 1;
        final int encryptedKeyLength = 2;

        final ByteBuffer byteBuffer = ByteBuffer.allocate(1 + FelverEnvelopeAead.FIELD_ENCRYPTED_LEN_BYTE);
        byteBuffer.put(algorithm);
        byteBuffer.putInt(encryptedKeyLength);

        final String wrongEncryptedString = Base64.getEncoder().encodeToString(byteBuffer.array());

        assertThrows(GeneralSecurityException.class, () -> fineEncryption.decrypt(wrongEncryptedString));
    }

    @Test
    void testEncryptionKeyUsageCorrectIncremented() throws Exception {
        assertEquals(0, encryptionKeyStorage.getEncryptionKey().getUsageCount(), "The key usage count was not correctly incremented during encryption.");

        for (int i = 1; i <= 3; i++) {
            fineEncryption.encrypt(UUID.randomUUID().toString());
            assertEquals(i, encryptionKeyStorage.getEncryptionKey().getUsageCount(), "The key usage count was not correctly incremented during encryption.");
        }
    }

    @Test
    void testInitWithKeyProvider() throws Exception {
        final KeyProvider keyProvider = mock(KeyProvider.class);
        when(keyProvider.generateNewEncryptionKey(any())).thenReturn(encryptionKey);
        when(keyProvider.getDecryptionKey(any(), eq(encryptionKey.getEncryptedKey()))).thenReturn(decryptionKey);

        final FelverEnvelopeAead fineEncryption = new FelverEnvelopeAead(keyProvider);

        final String randomString = UUID.randomUUID().toString();

        final String encrypted = fineEncryption.encrypt(randomString);
        fineEncryption.decrypt(encrypted);

        verify(keyProvider).generateNewEncryptionKey(any());
        verify(keyProvider).getDecryptionKey(any(), eq(encryptionKey.getEncryptedKey()));
    }
}
