package de.osp.fine.felver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

import com.google.auth.oauth2.GoogleCredentials;

@Tag("integration")
public class FelverEnvelopeAeadIntegrationTest {
    private void testEncryptDecrypt(FelverEnvelopeAead felverEnvelopeAead) throws Exception {
        final String randomString = UUID.randomUUID().toString();

        final String encrypted = felverEnvelopeAead.encrypt(randomString);
        final String decrypted = felverEnvelopeAead.decrypt(encrypted);

        assertNotEquals(randomString, encrypted, "Original string and encrypted string match.");
        assertEquals(randomString, decrypted, "Original string and decrypted string do not match.");
    }

    @Test
    void testKmsClientWithApplicationDefaultCredentials() throws Exception {
        final GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        final KeyProvider keyProvider = new KmsClientKeyProvider(credentials, System.getenv("TEST_PROJECT_ID"), System.getenv("TEST_LOCATION_ID"), System.getenv("TEST_KEYRING_ID"), System.getenv("TEST_KEY_ID"));
        final FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

        testEncryptDecrypt(felverEnvelopeAead);
    }

    @Test
    void testKmsClientWithCredentialsPath() throws Exception {
        final GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(new File(System.getenv("TEST_CREDENTIALS"))));
        final KeyProvider keyProvider = new KmsClientKeyProvider(credentials, System.getenv("TEST_PROJECT_ID"), System.getenv("TEST_LOCATION_ID"), System.getenv("TEST_KEYRING_ID"), System.getenv("TEST_KEY_ID"));
        final FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

        testEncryptDecrypt(felverEnvelopeAead);
    }

    @Test
    void testKmsProxyWithApplicationDefaultCredentials() throws Exception {
        final GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        final KeyProvider keyProvider = new KmsProxyKeyProvider(credentials, System.getenv("TEST_BASE_URL"), System.getenv("TEST_AUDIENCE"));
        final FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

        testEncryptDecrypt(felverEnvelopeAead);
    }

    @Test
    void testKmsProxyWithCredentialsPath() throws Exception {
        final GoogleCredentials credentials = GoogleCredentials.fromStream(new FileInputStream(new File(System.getenv("TEST_CREDENTIALS"))));
        final KeyProvider keyProvider = new KmsProxyKeyProvider(credentials, System.getenv("TEST_BASE_URL"), System.getenv("TEST_AUDIENCE"));
        final FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

        testEncryptDecrypt(felverEnvelopeAead);
    }

    @Test
    @EnabledIfEnvironmentVariable(named = "TEST_KEYRING_ID", matches = "demo")
    @EnabledIfEnvironmentVariable(named = "TEST_KEY_ID", matches = "integration-layer-demo-app")
    void testOldEncryptedContentCanStillBeDecrypted() throws Exception {
        final GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        final KeyProvider keyProvider = new KmsClientKeyProvider(credentials, System.getenv("TEST_PROJECT_ID"), System.getenv("TEST_LOCATION_ID"), System.getenv("TEST_KEYRING_ID"), System.getenv("TEST_KEY_ID"));
        final FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

        final String decrypted = felverEnvelopeAead.decrypt("AQAAAHEKJAAgxc3vhLBT8HNlrJHvBShe+Lq7avPNH26y4i5RACcbk/asexJJAAOMNtIq0YXXboWiMVOgy4cTH/PygD/ak2yzJqwvq4kaVWhDwmlpTNb+YpNCPNj7rXim9VA1YZdbIbcyqQOYwDhVbz6MCOi1p1lkYOmWQ6zeRW1vItid1i6EaOmhEgcknhiCp08c6pz1iMzyFCHEnFMI0pZMQlL+EFQC+kwekk/5rK89zn2yoWkqHg6ugHBc+FVykXUnQqmnqJs3hy+6NKylosvZ1g==");

        assertEquals("This is a message encrypted with the first released version. 👍", decrypted, "Older encrypted content was not decrypted correctly.");
    }
}
