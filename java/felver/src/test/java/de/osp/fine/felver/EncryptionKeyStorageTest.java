package de.osp.fine.felver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.util.Random;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EncryptionKeyStorageTest {
    private EncryptionKeyStorage encryptionKeyStorage;
    private KeyProvider keyProvider;
    private Clock clock;

    @BeforeEach
    void setUp() throws Exception {
        clock = mock(Clock.class);
        keyProvider = mock(KeyProvider.class);

        when(keyProvider.generateNewEncryptionKey(clock)).then((invocation) -> {
            final byte[] encryptedKey = UUID.randomUUID().toString().getBytes();
            final byte[] plaintextKey = new byte[32];
            new Random().nextBytes(plaintextKey);
            EncryptionKey encryptionKey = new EncryptionKey(Bytes.wrap(plaintextKey), Bytes.wrap(encryptedKey), clock);
            return encryptionKey;
        });

        encryptionKeyStorage = new EncryptionKeyStorage(keyProvider, 3L, clock);
    }

    @Test
    void testKeyReusedWithinRenewalTime() throws Exception {
        final Instant instant = Instant.ofEpochMilli(0);
        when(clock.instant()).thenReturn(instant);

        final EncryptionKey encryptionKey1 = encryptionKeyStorage.getEncryptionKey();
   
        when(clock.instant()).thenReturn(instant.plus(EncryptionKeyStorage.KEY_RENEWAL_AFTER_DURATION.minusSeconds(1)));

        final EncryptionKey encryptionKey2 = encryptionKeyStorage.getEncryptionKey();

        assertEquals(encryptionKey1, encryptionKey2, "The keys were not reused within the renewal time.");
    }

    @Test
    void testKeyNotReusedAfterRenewalTime() throws Exception {
        final Instant instant = Instant.ofEpochMilli(0);
        when(clock.instant()).thenReturn(instant);

        final EncryptionKey encryptionKey1 = encryptionKeyStorage.getEncryptionKey();

        when(clock.instant()).thenReturn(instant.plus(EncryptionKeyStorage.KEY_RENEWAL_AFTER_DURATION.plusSeconds(1)));

        final EncryptionKey encryptionKey2 = encryptionKeyStorage.getEncryptionKey();

        assertNotEquals(encryptionKey1, encryptionKey2, "The keys were reused after the renewal time.");
    }

    @Test
    void testKeyReusedWithinUsageLimit() throws Exception {
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));

        final EncryptionKey encryptionKey1 = encryptionKeyStorage.getEncryptionKey();

        for (int i = 0; i < 2; i++) {
            encryptionKey1.incrementUsageCount();
        }

        final EncryptionKey encryptionKey2 = encryptionKeyStorage.getEncryptionKey();

        assertEquals(encryptionKey1, encryptionKey2, "The keys were not reused within the usage limit.");
    }

    @Test
    void testKeyNotReusedAfterUsageLimit() throws Exception {
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));

        final EncryptionKey encryptionKey1 = encryptionKeyStorage.getEncryptionKey();

        for (int i = 0; i < 3; i++) {
            encryptionKey1.incrementUsageCount();
        }

        final EncryptionKey encryptionKey2 = encryptionKeyStorage.getEncryptionKey();

        assertNotEquals(encryptionKey1, encryptionKey2, "The keys were reused after usage limit was reached.");
    }
}
