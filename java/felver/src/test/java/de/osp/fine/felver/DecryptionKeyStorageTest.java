package de.osp.fine.felver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DecryptionKeyStorageTest {
    private DecryptionKeyStorage decryptionKeyStorage;
    private KeyProvider keyProvider;
    private Clock clock;

    @BeforeEach
    void setUp() throws Exception {
        clock = mock(Clock.class);
        keyProvider = mock(KeyProvider.class);

        when(keyProvider.getDecryptionKey(eq(clock), any())).then((invocation) -> {
            byte[] key = UUID.randomUUID().toString().getBytes();
            DecryptionKey decryptionKey = new DecryptionKey(Bytes.wrap(key), clock);
            return decryptionKey;
        });

        decryptionKeyStorage = new DecryptionKeyStorage(keyProvider, clock);
    }

    @Test
    void testKeyStoredInCacheWithinDeletionTime() throws Exception {
        final Instant instant = Instant.ofEpochMilli(0);
        when(clock.instant()).thenReturn(instant);

        final Bytes encryptedKey = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        decryptionKeyStorage.getDecryptionKey(encryptedKey);

        when(clock.instant()).thenReturn(instant.plus(DecryptionKeyStorage.KEY_DELETION_AFTER_DURATION.minusSeconds(1)));

        decryptionKeyStorage.getDecryptionKey(encryptedKey);

        verify(keyProvider).getDecryptionKey(eq(clock), eq(encryptedKey));
    }

    @Test
    void testKeyStoredInCacheBasedOnEncryptedKey() throws Exception {
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));

        final Bytes encryptedKey1 = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        decryptionKeyStorage.getDecryptionKey(encryptedKey1);

        final Bytes encryptedKey2 = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        decryptionKeyStorage.getDecryptionKey(encryptedKey2);

        verify(keyProvider).getDecryptionKey(eq(clock), eq(encryptedKey1));
        verify(keyProvider).getDecryptionKey(eq(clock), eq(encryptedKey2));
    }

    @Test
    void testKeyRemovedFromCacheAfterDeletionTime() throws Exception {
        final Instant instant = Instant.ofEpochMilli(0);
        when(clock.instant()).thenReturn(instant);

        final Bytes encryptedKey = Bytes.wrap(UUID.randomUUID().toString().getBytes());

        decryptionKeyStorage.getDecryptionKey(encryptedKey);

        when(clock.instant()).thenReturn(instant.plus(DecryptionKeyStorage.KEY_DELETION_AFTER_DURATION.plusSeconds(1)));

        decryptionKeyStorage.getDecryptionKey(encryptedKey);

        verify(keyProvider, times(2)).getDecryptionKey(eq(clock), eq(encryptedKey));
    }
}
