package de.osp.fine.felver;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class BytesTest {
    @Test
    public void testWrapAndGet() {
        final byte[] bytes = "test".getBytes();
        final Bytes wrappedBytes = Bytes.wrap(bytes);

        assertArrayEquals(bytes, wrappedBytes.get());
    }

    @Test
    public void testEqual() {
        final byte[] bytes1 = "test".getBytes();
        final Bytes wrappedBytes1 = Bytes.wrap(bytes1);

        final byte[] bytes2 = "test".getBytes();
        final Bytes wrappedBytes2 = Bytes.wrap(bytes2);

        assertTrue(wrappedBytes1.equals(wrappedBytes2));
    }

    @Test
    public void testNotEqual() {
        final byte[] bytes1 = "test1".getBytes();
        final Bytes wrappedBytes1 = Bytes.wrap(bytes1);

        final byte[] bytes2 = "test2".getBytes();
        final Bytes wrappedBytes2 = Bytes.wrap(bytes2);

        assertFalse(wrappedBytes1.equals(wrappedBytes2));
    }
}
