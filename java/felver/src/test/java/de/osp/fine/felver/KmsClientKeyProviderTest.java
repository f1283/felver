package de.osp.fine.felver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Clock;
import java.time.Instant;
import java.util.UUID;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.MockedStatic;

import com.google.api.gax.core.CredentialsProvider;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.kms.v1.CryptoKeyName;
import com.google.cloud.kms.v1.DecryptResponse;
import com.google.cloud.kms.v1.EncryptResponse;
import com.google.cloud.kms.v1.KeyManagementServiceClient;
import com.google.cloud.kms.v1.KeyManagementServiceSettings;
import com.google.protobuf.ByteString;

public class KmsClientKeyProviderTest {
    private final String projectId = "test-project";
    private final String locationId = "test-location";
    private final String keyRingId = "test-key-ring";
    private final String keyId = "test-key";

    private final CryptoKeyName cryptoKeyName = CryptoKeyName.of(projectId, locationId, keyRingId, keyId);

    private KmsClientKeyProvider kmsClientKeyProvider;
    private Clock clock;
    private KeyManagementServiceClient kmsClient;
    private KeyGenerator keyGenerator;

    @BeforeEach
    void setUp() throws Exception {
        kmsClient = mock(KeyManagementServiceClient.class);
        clock = mock(Clock.class);
        keyGenerator = mock(KeyGenerator.class);
        
        kmsClientKeyProvider = new KmsClientKeyProvider(kmsClient, keyGenerator, cryptoKeyName);

        when(clock.instant()).thenReturn(Instant.ofEpochMilli(0));
    }

    @Test
    void testGenerateNewEncryptionKey() throws Exception {
        final byte[] plaintextKey = UUID.randomUUID().toString().getBytes();
        final byte[] encryptedKey = UUID.randomUUID().toString().getBytes();

        when(keyGenerator.generateKey()).thenReturn(new SecretKeySpec(plaintextKey, "AES"));
        when(kmsClient.encrypt(cryptoKeyName, ByteString.copyFrom(plaintextKey)))
                .thenReturn(EncryptResponse.newBuilder().setCiphertext(ByteString.copyFrom(encryptedKey)).build());

        final EncryptionKey encryptionKey = kmsClientKeyProvider.generateNewEncryptionKey(clock);

        assertEquals(Bytes.wrap(plaintextKey), encryptionKey.getPlaintextKey());
        assertEquals(Bytes.wrap(encryptedKey), encryptionKey.getEncryptedKey());
    }

    @Test
    void testGetDecryptionKey() throws Exception {
        final byte[] plaintextKey = UUID.randomUUID().toString().getBytes();
        final byte[] encryptedKey = UUID.randomUUID().toString().getBytes();

        when(kmsClient.decrypt(cryptoKeyName, ByteString.copyFrom(encryptedKey)))
                .thenReturn(DecryptResponse.newBuilder().setPlaintext(ByteString.copyFrom(plaintextKey)).build());

        final DecryptionKey decryptionKey = kmsClientKeyProvider.getDecryptionKey(clock, Bytes.wrap(encryptedKey));

        assertEquals(Bytes.wrap(plaintextKey), decryptionKey.getPlaintextKey());
    }

    @Test
    void testInitKmsClientProvider() throws Exception {
        try (MockedStatic<KeyManagementServiceClient> kmsClientStaticMock = mockStatic(KeyManagementServiceClient.class)) {
            final GoogleCredentials credentials = new CredentialsProviderMock();
            final ArgumentCaptor<KeyManagementServiceSettings> settingsCaptor = ArgumentCaptor.forClass(KeyManagementServiceSettings.class);

            kmsClientStaticMock.when(() -> KeyManagementServiceClient.create(settingsCaptor.capture())).thenReturn(kmsClient);

            final KmsClientKeyProvider kmsClientKeyProvider = new KmsClientKeyProvider(credentials, projectId, locationId,
                    keyRingId, keyId);

            assertEquals(credentials, settingsCaptor.getValue().getCredentialsProvider().getCredentials());

            when(kmsClient.encrypt(eq(cryptoKeyName), any())).thenReturn(EncryptResponse.newBuilder().build());

            final EncryptionKey encryptionKey1 = kmsClientKeyProvider.generateNewEncryptionKey(clock);
            final EncryptionKey encryptionKey2 = kmsClientKeyProvider.generateNewEncryptionKey(clock);

            assertNotEquals(encryptionKey1.getPlaintextKey(), encryptionKey2.getPlaintextKey());
        }
    }

    public class CredentialsProviderMock extends GoogleCredentials implements CredentialsProvider {
        @Override
        public Credentials getCredentials() throws IOException {
            return mock(Credentials.class);
        }
    }
}
