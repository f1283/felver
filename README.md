# Felver

This library provides the necessary encryption and decryption methods for the field-by-field encryption of personal data when exchanging messages via a Kafka cluster. The purpose of this library is to use a common concept so that all communication partners can exchange messages in a compatible manner.

## What is the basic principle?

This library lets you perform client-side encryption in the following way:
* It generate a data encryption key (either locally or using the KMS-Proxy).
* The data is encrypted locally using AES-256-GCM using the data encryption key.
* The data encryption key is encrypted (either by using Google's Key Management System or using the KMS-Proxy).
* The encrypted data encryption key is attached to the encrypted data in a special way.

The library also provides a method for decrypting data that was encrypted in this way.

The data encryption key is reused for a maximum of 6 hours. This reduces the number of external requests to Google's Key Management System to encrypt the data encryption key or to the KMS-Proxy to generate a new data encryption key.

It's similar with decryption. The encrypted data encryption keys received are decrypted using Google's Key Management System or the KMS-Proxy and also temporarily stored in the case that further messages arrive that have been encrypted with the same data encryption key.

The end user of this library does not have to do more than configure the KMS key or the KMS-Proxy endpoint to be used and provide the necessary credentials. The library will do the rest.

## What is the difference to existing libraries?

Envelope encryption and the use of KMS for implementation are nothing new. Why is a separate library provided?

* More generalized libraries often offer a range of functions that is not required in this special scenario. In this way, we avoid dependencies that are not required.
* They also offer configuration options which, if incorrectly configured, can lead to incompatibilities when exchanging encrypted messages with other systems and can sometimes lead to reduced security due to incorrect use. Apart from specifying the KMS key to be used, this library offers no further configuration options.
* Solutions in which the data key is cached and used multiple times are rare. Usually the data key is regenerated for each encryption. For reasons of performance, however, a different approach should be preferred. It makes sense to specify a time when keys are rotated directly in the library, since there is a mutual interest that the keys are not exchanged too often, but also not too rarely.
* The library follows the KISS principle. Everyone should be able to understand how the compatible encryption of strings is done using other standard libraries. That is why too complex constructions are avoided. In addition, the library should be a template so that applications can develop a solution that is compatible with this variant, even in other programming languages.
* When developing the library, it was important that this logic can also be implemented in other programming languages, for example that there are well-maintained, modern standard libraries there. As soon as new standards are fully established, it is recommended to make adjustments to the library.
* Care was taken to ensure that the template does not provide any incentive to go wrong with the implementation. Instead of making something absolutely safe, sometimes very small limitations are accepted (see below) in order to prevent more complicated solutions from leading to something serious being done wrong during implementation. Nevertheless, all common requirements and current rules of technology are fully taken into account. The protection goal in our scenario is achieved.

## Usage

### Add depdency

You can integrate the current release of this library as a project dependency using the following configuration:

**Gradle**
```
repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/31081315/packages/maven'
        name 'gitlab-maven-felver'
    }
}

dependencies {
  implementation 'de.osp.fine:felver:2.1.0'
}
```

**Maven**
```
<repositories>
  <repository>
    <id>gitlab-maven-felver</id>
    <url>https://gitlab.com/api/v4/projects/31081315/packages/maven</url>
  </repository>
</repositories>

<distributionManagement>
  <repository>
    <id>gitlab-maven-felver</id>
    <url>https://gitlab.com/api/v4/projects/31081315/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven-felver</id>
    <url>https://gitlab.com/api/v4/projects/31081315/packages/maven</url>
  </snapshotRepository>
</distributionManagement>

<dependencies>
  <dependency>
    <groupId>de.osp.fine</groupId>
    <artifactId>felver</artifactId>
    <version>2.1.0</version>
  </dependency>
</dependencies>
```

### Initializing

The encryption and decryption methods are offered by the `FelverEnvelopeAead` class. For maximum efficiency, applications should use a single instance of `FelverEnvelopeAead`. The keys are stored in this object so that they can be reused for a limited period of time. Recreating the object each time when a string is encrypted or decrypted has a negative impact on performance.

When the constructor is called, a `KeyProvider` instance must be provided that defines which functionality is used to generate the data encryption key (for encryption) and to decrypt an encrypted data encryption key (for decryption). There are currently two implementations of the `KeyProvider` interface:

```
// use REST API endpoints of a KMS-Proxy
KmsProxyKeyProvider keyProvider = new KmsProxyKeyProvider(googleGredentials, baseUrl, audience);

// use a key in Google's Key Management System
KmsClientKeyProvider keyProvider = new KmsClientKeyProvider(googleGredentials, projectId, locationId, keyRingId, keyId);
```

The `googleCredentials` object must be of type `com.google.auth.oauth2.GoogleCredentials`. There are two common ways to create it:

Use the Google-defined Application Default Credentials (ADC) flow to automatically find credentials based on the application environment.

```
GoogleCredentials googleGredentials = GoogleCredentials.getApplicationDefault();
```

The ADC flow consists of the following steps:
* If the environment variable `GOOGLE_APPLICATION_CREDENTIALS` is set, ADC uses the service account key or configuration file that the variable points to. (for example use `export GOOGLE_APPLICATION_CREDENTIALS="CREDENTIALS_PATH"`)
* If the environment variable isn't set, ADC uses the service account that is attached to the resource that is running the code. 
* If ADC can't use any of the above credentials, an error occurs.

If you do not want to use the ADC flow (e.g. because you use different credentlials for different purposes in your application at the same time), you can alternatively specify the file path:

```
GoogleCredentials googleGredentials = GoogleCredentials.fromStream(new FileInputStream(new File(pathToCredentialsFile)));
```

After creating a `KeyProvider` instance, the constructor of `FelverEnvelopeAead` can be called and the object can be used for encryption or decryption:

```
FelverEnvelopeAead felverEnvelopeAead = new FelverEnvelopeAead(keyProvider);

String encryptedValue = felverEnvelopeAead.encrypt("Hello World!");

String decryptedValue = felverEnvelopeAead.decrypt("AQAAAHEKJAAgxc3v881mTp2jdxkYVnPRUkHM9BNPYShnPF2afqjguyI9/RJJAAOMNtIgQjoAUNUkY43zXeBJhcKV72enbylEttmOSqRnItRcwzaIr/L0/VrhvenyeNvxxf/TcEgdXXLCL+kweNOyInsqmgOnn7Et3oJSgoy+Z/uQP1boInSExp0+exhvsVy5VbTmzqGFNu9X41Xdew==");
```

## Used algorithms and limitations

The first byte determines the algorithm used and the structure of the rest of the message. Currently only the value "1" is accepted, as there is no other variant so far. This value describes the following characteristics:

### Structure of the byte array

![](doc/assets/bytearray.png)

### Encryption algorithm

AES-256-GCM without padding is used as the encryption algorithm. When used correctly, it is a very secure, high-performance and established algorithm that not only ensures confidentiality but also the integrity of the message. However, there are also limitations that must be taken into account.

The library uses the same 256 bit strong key multiple times for encryption. That is absolutely fine under certain circumstances. It is particularly important that the same key and the same initialization vector (IV) are not used multiple times in the same combination when using AES-GCM for encryption. The pair (key, IV) must be unique for all encryptions.

If not, the following happens:
* If an IV is reused and one plaintext is known, the message with the same IV can be decrypted only by using XOR without knowing the key.
* Significant information on the authentication key, which is used to ensure integrity, is leaked. The integrity of all messages that have been encrypted with the same key is affected.

There are several options for uniquely choosing the 12 byte long IV:
* You can use a counter that is counted up after each encrypted message. The IV is allowed to be predictable in AES-GCM. In this way you have the greatest possible security and a very large number of messages ($`2^{96}`$) that can be encrypted with the same key. However, this requires a bit more effort to implement, since the counter has to be used correctly.
* You can calculate a deterministic value from the plain text and use it as part of the IV. Of course, collisions are possible here, which depend on the specific construction.
* You can generate the IV randomly at each encryption using a cryptographically strong random number generator.

The library generates the IV randomly, because it reduces the number of implementation errors and does not increase the complexity of the application. NIST recommends that when using randomly generated IVs, no more than $`2^{32}`$ IVs should be created for the same key. Then the probability that at least two IVs are equal is $`2^{-33} ≈ 0,00000001 \%`$. This is an acceptable risk in practice. The library generates a new encryption key when one of the following events occurs: The current key was generated more than 6 hours ago or the key was used $`2^{32}`$ times for encryption.
